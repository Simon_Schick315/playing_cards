#include<iostream>
#include<conio.h>

using namespace std;

enum class Suit
{
	Clubs,
	Hearts,
	Spades,
	Diamonds
};

enum class Rank
{
	TWO = 2,
	THREE = 3,
	FOUR = 4,
	FIVE = 5,
	SIX = 6,
	SEVEN = 7,
	EIGHT = 8,
	NINE = 9,
	TEN = 10,
	JACK,
	Queen,
	KING,
	ACE
};

struct Card
{
	Suit suit;
	Rank rank;
};

int main()
{
	Card test;
	
	test.rank = Rank::KING;
	test.suit = Suit::Spades;

	(void)_getch();
	return 0;
}